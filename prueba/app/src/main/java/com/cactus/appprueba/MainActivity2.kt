package com.cactus.appprueba

import android.os.Bundle
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.cactus.appprueba.databinding.ActivityMain2Binding

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMain2Binding.inflate(layoutInflater)
        enableEdgeToEdge()
        setContentView(binding.root)

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        val recycler = binding.idRecycler
        recycler.layoutManager = GridLayoutManager(binding.idRecycler.context, 1)
        val adapter = EjemploAdapter()

        adapter.setOnItemClickListener {

            Toast.makeText(
                applicationContext,
                it.titulo,
                Toast.LENGTH_LONG
            ).show()
        }

        adapter.submitList(getList())
        recycler.adapter = adapter

    }

    fun getList(): MutableList<EjemploModelo>{
        var list = mutableListOf<EjemploModelo>()
        list.add(EjemploModelo("CIENCIAS DE LA COMPUTACION", "MATERIA PRIMER SEMESTRE",
            "GALAVIZ", "https://aniversario.fciencias.unam.mx/imgs/eventos/04/06/f1_gal.jpg"))
        list.add(EjemploModelo("ESTRUCTURAS DE DATOS", "MATERIA SEGUNDO SEMESTRE",
            "CANEK", "https://i.ytimg.com/vi/cLRmAIdUhug/maxresdefault.jpg"))
        list.add(EjemploModelo("Lenguajes de programacion", "MATERIA QUINTO SEMESTRE",
            "KARLA PULIDO", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTHPrjHWnL7qnfE8B0JxGhitUCRUwxL6Q9DpVSbojLKjw&s"))

        return list
    }
}