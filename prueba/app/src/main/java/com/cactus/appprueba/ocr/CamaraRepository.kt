package com.cactus.appprueba.ocr

import android.util.Log
import androidx.annotation.OptIn
import androidx.camera.core.ExperimentalGetImage
import androidx.camera.core.ImageProxy
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions

class CamaraRepository {

    private val TAG = "TAG_OCR"
    @OptIn(ExperimentalGetImage::class)
    fun obtenerOCR(imageProxy: ImageProxy): List<String>{

        var listaResultados = mutableListOf<String>()

        val mediaImage = imageProxy.image
        if(mediaImage != null){

            val inputImageProxy = InputImage.fromMediaImage(mediaImage, imageProxy.imageInfo.rotationDegrees)

            val recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)
            val result = recognizer.process(inputImageProxy).
            addOnSuccessListener { visionText ->

                val resultText = visionText.text

                Log.d(TAG, "sin errores ")
                Log.d(TAG, resultText)

                for (block in visionText.textBlocks) {
                    val blockText = block.text
                    val blockCornerPoints = block.cornerPoints
                    val blockFrame = block.boundingBox
                    for (line in block.lines) {
                        val lineText = line.text
                        val lineCornerPoints = line.cornerPoints
                        val lineFrame = line.boundingBox
                        for (element in line.elements) {
                            val elementText = element.text
                            val elementCornerPoints = element.cornerPoints
                            val elementFrame = element.boundingBox
                            listaResultados.add(elementText)
                            Log.d(TAG, elementText)
                        }
                    }
                }
                Log.d(TAG, "SIN ERRORes")
                imageProxy.close()
            }
                .addOnFailureListener { e ->
                    Log.d(TAG, "UN ERROR")
                    Log.e(TAG, e.toString())
                    imageProxy.close()
                }
        }


        Log.d(TAG, "finaliza")
        return  listaResultados

    }




}