package com.cactus.appprueba

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cactus.appprueba.databinding.AdapterEjemploBinding
import com.squareup.picasso.Picasso

class EjemploAdapter: ListAdapter<EjemploModelo, EjemploAdapter.EjemploViewHolder>(DiffCallback)   {

    companion object DiffCallback : DiffUtil.ItemCallback<EjemploModelo>() {

        override fun areItemsTheSame(oldItem: EjemploModelo, newItem: EjemploModelo): Boolean {
            return oldItem.titulo == newItem.titulo
        }

        override fun areContentsTheSame(oldItem: EjemploModelo, newItem: EjemploModelo): Boolean {
            return oldItem.titulo == newItem.titulo
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EjemploViewHolder {
        val binding = AdapterEjemploBinding.inflate(LayoutInflater.from(parent.context))
        return EjemploViewHolder(binding)
    }

    /**
     * We binding the data to a viewHolder
     */
    override fun onBindViewHolder(currencyViewHolder: EjemploViewHolder, position: Int) {
        val currency = getItem(position)
        currencyViewHolder.bind(currency)
    }

    /**
     * this property that is a function given from MainActivity
     */
    private var onItemClickListener: ((EjemploModelo) -> Unit)? = null
    fun setOnItemClickListener(onItemClickListener: (EjemploModelo) -> Unit){
        this.onItemClickListener = onItemClickListener
    }

    inner class EjemploViewHolder(private val binding: AdapterEjemploBinding) : RecyclerView.ViewHolder(binding.root){

        /**
         * Function to bind currency data with the adapterListCurrency views
         */
        fun bind(ejemploModelo: EjemploModelo){
            binding.idTituloPublicacion.text = ejemploModelo.titulo
            binding.idContenidoPublicacion.text = ejemploModelo.contenido
            binding.idAutor.text = ejemploModelo.autor
            Picasso.get()
                .load(ejemploModelo.imageUrl)
                .placeholder( R.drawable.water )
                .error(R.drawable.water)
                .into(binding.idImageView);
            binding.idRecyclerPublicaciones.setOnClickListener {
                onItemClickListener?.invoke(ejemploModelo)
            }
        }
    }

}