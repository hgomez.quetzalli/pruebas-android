package com.cactus.appprueba.ocr

import androidx.camera.core.ImageProxy
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.mlkit.vision.common.InputImage
import kotlinx.coroutines.launch

class CamaraViewModel : ViewModel(){

    private val _stringLista = MutableLiveData<List<String>>()
    val stringLista : LiveData<List<String>>
        get() = _stringLista

    fun getOCR(imageProxy: ImageProxy){
        viewModelScope.launch {
            val camaraRepository = CamaraRepository()
            _stringLista.value = camaraRepository.obtenerOCR(imageProxy)
        }
    }
}