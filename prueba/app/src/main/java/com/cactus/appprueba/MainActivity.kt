package com.cactus.appprueba

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.cactus.appprueba.databinding.ActivityMainBinding
import com.cactus.appprueba.ocr.CamaraActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        enableEdgeToEdge()
        setContentView(binding.root)

        var click = false
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        binding.idButton.setOnClickListener(View.OnClickListener {
            if(binding.idSwitch.isChecked){
                if(click){
                    binding.idTextView.text = "hola mundo"
                    click = false
                }else{
                    binding.idTextView.text = "hello world"
                    click = true
                }
            }else{
                Toast.makeText(applicationContext, "no se puede cambiar", Toast.LENGTH_SHORT).show()
            }

        })

        binding.idButton1.setOnClickListener(View.OnClickListener {
            val intent = Intent(applicationContext, MainActivity3::class.java)
            intent.putExtra("info", "de pantalla 1 a 3")
            startActivity(intent)
        })

        binding.idButton2.setOnClickListener(View.OnClickListener {
            val intent = Intent(applicationContext, MainActivity2::class.java)
            startActivity(intent)
        })

        binding.idBtnCamera.setOnClickListener(View.OnClickListener {
            val intent = Intent(applicationContext, CamaraActivity::class.java)
            startActivity(intent)
        })
    }

}