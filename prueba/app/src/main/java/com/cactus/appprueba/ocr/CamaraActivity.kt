package com.cactus.appprueba.ocr

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.annotation.OptIn
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.CameraSelector
import androidx.camera.core.ExperimentalGetImage
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import com.cactus.appprueba.databinding.ActivityCamaraBinding
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import java.nio.ByteBuffer
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

typealias LumaListener = (imageProxi: ImageProxy ) -> Unit

class CamaraActivity : AppCompatActivity() {

    private lateinit var viewBinding: ActivityCamaraBinding
    private lateinit var cameraExecutor: ExecutorService

    val viewModel : CamaraViewModel by viewModels()
    lateinit var imageProxy: ImageProxy

    private val TAG = "soyUnTag"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityCamaraBinding.inflate(layoutInflater)
        enableEdgeToEdge()
        setContentView(viewBinding.root)

        startCamera()

        viewModel.stringLista.observe(this){
                listaString ->
            //Toast.makeText(applicationContext, "regresa", Toast.LENGTH_SHORT).show()
            if(!listaString.isNullOrEmpty()){
                viewBinding.idTextViewNombre.text = listaString.toString()

            }
            //this.imageProxy.close()

        }

        cameraExecutor = Executors.newSingleThreadExecutor()

    }

    @OptIn(ExperimentalGetImage::class)
    private fun startCamera() {
        /*
        Crea una instancia de ProcessCameraProvider. Se usa para vincular el ciclo de vida de las cámaras
        al propietario del ciclo de vida. Esto elimina la tarea de abrir y cerrar la cámara, ya que
        CameraX se adapta al ciclo de vida.
         */
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)

        /*
        Agrega un objeto de escucha a cameraProviderFuture. Agrega un elemento Runnable como primer argumento.
        Agrega ContextCompat.getMainExecutor() como segundo argumento.
        Se mostrará un Executor que se ejecutará en el subproceso principal.
         */
        cameraProviderFuture.addListener({
            /*
            En el Runnable, agrega un ProcessCameraProvider. Esto se usa para vincular el ciclo de vida de
            nuestra cámara al LifecycleOwner dentro del proceso de la aplicación.
             */
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            /*
            Inicializa nuestro objeto Preview, llama a su compilación, obtén un proveedor de
            plataforma desde el visor y, luego, configúralo en la vista previa.
            */
            val preview = Preview.Builder()
                .build()
                .also {
                    it.setSurfaceProvider(viewBinding.viewFinder.surfaceProvider)
                }


            val imageAnalyzer = ImageAnalysis.Builder()
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .build()
                .also {
                    it.setAnalyzer(cameraExecutor, LuminosityAnalyzer ({ luma ->
                        //Log.d(TAG, "Average luminosity: $luma")
                        viewModel.getOCR(luma)
                    }, viewModel))
                }

/*
            val imageAnalyzer = ImageAnalysis.Builder()
                // enable the following line if RGBA output is needed.
                // .setOutputImageFormat(ImageAnalysis.OUTPUT_IMAGE_FORMAT_RGBA_8888)
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .build()
            imageAnalyzer.setAnalyzer(cameraExecutor) { imageProxy ->

                viewModel.getOCR(imageProxy)
                Log.d("logIma", "inicia ima 2")
                this.imageProxy = imageProxy

                //imageProxy.close()
            }
*/

            /*
            Crea un objeto CameraSelector y selecciona DEFAULT_BACK_CAMERA.
             */
            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            /*
            Crea un bloque try. Dentro de ese bloque, asegúrate de que no haya nada vinculado a cameraProvider y,
            luego, vincula nuestro cameraSelector y el objeto de vista previa a cameraProvider.
             */
            try {

                cameraProvider.unbindAll()
                cameraProvider.bindToLifecycle(
                    this, cameraSelector, preview, imageAnalyzer)

            } catch(exc: Exception) {
                Log.e(TAG, "Use case binding failed", exc)
            }

        }, ContextCompat.getMainExecutor(this))
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraExecutor.shutdown()
    }

    private class LuminosityAnalyzer(private val listener: LumaListener, val viewModel : CamaraViewModel) : ImageAnalysis.Analyzer {

        @OptIn(ExperimentalGetImage::class)
        override fun analyze(imageProxy: ImageProxy) {

            Log.d("logIma", "inicia ima")

            /*
            val mediaImage = imageProxy.image
            if (mediaImage != null) {
                val image = InputImage.fromMediaImage(mediaImage, imageProxy.imageInfo.rotationDegrees)
                // Pass image to an ML Kit Vision API
                // ...
                Log.d("logIma", "inicia ima")
                viewModel.getOCR(imageProxy)
                //imageProxy.close()
            }*/

            listener(imageProxy)
            //imageProxy.close()
        }
    }

}

